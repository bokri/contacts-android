package com.demo.contacts.network;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.util.Log;

import com.demo.contacts.constants.Constants;
import com.demo.contacts.models.Contact;
import com.demo.contacts.utils.Utils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Aymen Bokri
 * @since 17.08.2017
 */

public class ContactsFetcherTask extends AsyncTask<Void, Void, List<Contact>>
{

  @Override
  public List<Contact> doInBackground(Void... params) {
    try {


      JSONObject json = Utils.readJsonFromUrl(Constants.CONTACTS_BASE_URL);

      if(json != null && json.getJSONArray("contacts") != null  && json.getJSONObject("me") != null ) {

        JSONArray jsonContacts = json.getJSONArray("contacts");
        List<Contact> contacts = new ArrayList<Contact>();

        for (int i=0; i < jsonContacts.length(); i++) {

          contacts.add(Utils.getContactFromJSON(jsonContacts.getJSONObject(i)));
        }

        contacts.add(Utils.getContactFromJSON(json.getJSONObject("me")));

        return contacts;
      }

    } catch (Exception e) {
      Log.e(getClass().getName(), "Error while fetching or parsing data");
    }
    return new ArrayList<Contact>();
  }
}
