package com.demo.contacts.network;

import android.os.AsyncTask;
import android.util.Log;

import com.demo.contacts.constants.Constants;
import com.demo.contacts.models.Contact;
import com.demo.contacts.utils.Utils;
import org.json.JSONObject;

/**
 * @author Aymen Bokri
 * @since 17.08.2017
 */

public class MyContactFetcherTask
    extends AsyncTask<Void, Void, Contact>
{

  @Override
  public Contact doInBackground(Void... params) {
    try {


      JSONObject json = Utils.readJsonFromUrl(Constants.CONTACTS_BASE_URL);

      if(json != null && json.getJSONObject("me") != null ) {

       return Utils.getContactFromJSON(json.getJSONObject("me"));

      }

    } catch (Exception e) {
      Log.e(getClass().getName(), "Error while fetching or parsing data");
    }
    return null;
  }
}
