package com.demo.contacts.network;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.demo.contacts.utils.Utils;

/**
 * @author Aymen Bokri
 * @since aymenbokri
 */

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap>
{

  protected Bitmap doInBackground(String... urls) {
    String url = urls[0];

    try {
      return Utils.LoadImageFromWebOperations(url);
    } catch (Exception e) {
      Log.e("Error", e.getMessage());
      e.printStackTrace();
    }
    return null;
  }
}