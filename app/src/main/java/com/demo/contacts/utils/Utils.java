package com.demo.contacts.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.demo.contacts.models.Contact;
import com.demo.contacts.models.Location;
import com.demo.contacts.models.Name;
import com.demo.contacts.models.Picture;
import com.demo.contacts.network.ContactsFetcherTask;
import com.demo.contacts.network.MyContactFetcherTask;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Aymen Bokri
 * @since aymenbokri
 */

public class Utils
{
  private static String readAll(Reader rd) throws IOException
  {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException
  {
    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONObject json = new JSONObject(jsonText);
      return json;

    } finally {
      is.close();
    }
  }

  public static Contact getContactFromJSON(JSONObject jsonContact) throws JSONException
  {

    Name name = new Name(jsonContact.getJSONObject("name").getString("title"), jsonContact.getJSONObject("name").getString("first"), jsonContact.getJSONObject("name").getString("last"));
    Location location = new Location(jsonContact.getJSONObject("location").getString("street"), jsonContact.getJSONObject("location").getString("city"), jsonContact.getJSONObject("location").getString("state"), jsonContact.getJSONObject("location").getString("zip"));
    Picture picture = new Picture(jsonContact.getJSONObject("picture").getString("large"), jsonContact.getJSONObject("picture").getString("medium"), jsonContact.getJSONObject("picture").getString("thumbnail"));
    Contact contact = new Contact(jsonContact.getString("gender"), name, location, jsonContact.getString("email"), jsonContact.getString("username"), jsonContact.getString("password"), jsonContact.getString("phone"), jsonContact.getString("cell"), jsonContact.getString("SSN"), picture);

    return contact;
  }

  public static Bitmap LoadImageFromWebOperations(String src) {
    try {
      URL url = new URL(src);
      HttpURLConnection connection = (HttpURLConnection)url.openConnection();
      connection.setDoInput(true);
      connection.connect();
      InputStream inputStream = connection.getInputStream();
      Bitmap myBitmap = BitmapFactory.decodeStream(inputStream);
      return  myBitmap;
    }catch (Exception e){
      e.printStackTrace();
      return null;
    }
  }

  public static boolean contactExists(String name)
  {
    try
    {
      ContactsFetcherTask task = new ContactsFetcherTask();
      task.execute();
      List<Contact> contactsList = task.get();
      Contact contact = new Contact();
      contact.name.first = name;

      if(contactsList.contains(contact))
        return true;
      else
        return false;
    }
    catch (Exception e)
    {
     return false;
    }

  }

  public static Contact getMyCard()
  {
    try {

      MyContactFetcherTask task = new MyContactFetcherTask();
      task.execute();

      return task.get();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;

  }

}
