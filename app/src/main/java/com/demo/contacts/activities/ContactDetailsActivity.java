package com.demo.contacts.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo.contacts.R;
import com.demo.contacts.models.Contact;
import com.demo.contacts.network.DownloadImageTask;

public class ContactDetailsActivity
    extends AppCompatActivity
{

  ImageView profilePhoto;
  TextView name;
  TextView phone;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contact_details);

      try
      {
        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
          Contact contact = (Contact) extras.getSerializable("CONTACT");

          profilePhoto = (ImageView) findViewById(R.id.profilePhoto);
          name = (TextView) findViewById(R.id.name);
          phone = (TextView) findViewById(R.id.phone);

          DownloadImageTask task = new DownloadImageTask();
          task.execute(contact.picture.thumbnail);
          Bitmap bitmap = task.get();
          profilePhoto.setImageBitmap(bitmap);
          name.setText(contact.name.first);
          phone.setText(contact.phone);
        }
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

  }
}
