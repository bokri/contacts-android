package com.demo.contacts.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.demo.contacts.R;
import com.demo.contacts.models.Contact;
import com.demo.contacts.utils.Utils;

public class LoginActivity
    extends AppCompatActivity implements OnClickListener
{

  EditText username;
  EditText password;
  Button login;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    username = (EditText) findViewById(R.id.username);
    password = (EditText) findViewById(R.id.password);
    login = (Button) findViewById(R.id.login);

    login.setOnClickListener(this);
  }

  @Override
  public void onClick(View view)
  {
    if(view == login) {

      String usernameText = username.getText().toString();
      String passwordText = password.getText().toString();

      Contact myContact = Utils.getMyCard();
      if(myContact != null) {

        if(usernameText.equals(myContact.username) && passwordText.equals(myContact.password)) {

          Intent intent = new Intent(LoginActivity.this, MainActivity.class);
          startActivity(intent);
          this.finish();

        } else {
          login.setError("User not found!");
        }
      }


    }
  }
}
