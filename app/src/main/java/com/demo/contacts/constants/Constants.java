package com.demo.contacts.constants;

/**
 * @author Aymen Bokri
 * @since aymenbokri
 */

public interface Constants
{
  public static final String CONTACTS_BASE_URL  = "http://www.storage42.com/contacts.json";
}
