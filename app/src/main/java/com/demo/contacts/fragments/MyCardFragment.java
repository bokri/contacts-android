package com.demo.contacts.fragments;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo.contacts.R;
import com.demo.contacts.models.Contact;
import com.demo.contacts.network.DownloadImageTask;
import com.demo.contacts.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCardFragment
    extends Fragment
{

  ImageView profilePhoto;
  TextView name;
  TextView phone;


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState)
  {
    View rootView = inflater.inflate(R.layout.activity_contact_details, container, false);

    try
    {
        profilePhoto = (ImageView) rootView.findViewById(R.id.profilePhoto);
        name = (TextView) rootView.findViewById(R.id.name);
        phone = (TextView) rootView.findViewById(R.id.phone);

        Contact myContact = Utils.getMyCard();

      if(myContact != null)
      {

        DownloadImageTask task = new DownloadImageTask();
        task.execute(myContact.picture.thumbnail);
        Bitmap bitmap = task.get();
        profilePhoto.setImageBitmap(bitmap);
        name.setText(myContact.name.first);
        phone.setText(myContact.phone);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    return rootView;
  }

}
