package com.demo.contacts.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.contacts.R;
import com.demo.contacts.activities.ContactDetailsActivity;
import com.demo.contacts.models.Contact;
import com.demo.contacts.network.ContactsFetcherTask;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ContactsFragment
    extends Fragment
{

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p/>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnListFragmentInteractionListener
  {

    // TODO: Update argument type and name
    void onListFragmentInteraction(Contact item);
  }

  // TODO: Customize parameter argument names
  private static final String ARG_COLUMN_COUNT = "column-count";

  // TODO: Customize parameter initialization
  @SuppressWarnings("unused")
  public static ContactsFragment newInstance(int columnCount)
  {
    ContactsFragment fragment = new ContactsFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_COLUMN_COUNT, columnCount);
    fragment.setArguments(args);
    return fragment;
  }

  // TODO: Customize parameters
  private int mColumnCount = 1;

  private OnListFragmentInteractionListener mListener;

  /**
   * Mandatory empty constructor for the fragment manager to instantiate the
   * fragment (e.g. upon screen orientation changes).
   */
  public ContactsFragment()
  {
  }

  @Override
  public void onAttach(Context context)
  {
    super.onAttach(context);
    if (context instanceof OnListFragmentInteractionListener)
    {
      mListener = (OnListFragmentInteractionListener) context;
    }
    else
    {
      mListener = new OnListFragmentInteractionListener() {
        @Override
        public void onListFragmentInteraction(Contact item)
        {
          Intent intent = new Intent(getActivity(), ContactDetailsActivity.class);
          intent.putExtra("CONTACT", item);
          startActivity(intent);
        }
      };

    }
  }

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    if (getArguments() != null)
    {
      mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

    // Set the adapter
    if (view instanceof RecyclerView)
    {
      Context context = view.getContext();
      RecyclerView recyclerView = (RecyclerView) view;
      if (mColumnCount <= 1)
      {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
      }
      else
      {
        recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
      }

      List<Contact> contactsList = new ArrayList<Contact>();
      try
      {
        ContactsFetcherTask task = new ContactsFetcherTask();
        task.execute();
        contactsList = task.get();
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }finally
      {
        recyclerView.setAdapter(new MyContactRecyclerViewAdapter(contactsList, mListener));
      }


    }
    return view;
  }

  @Override
  public void onDetach()
  {
    super.onDetach();
    mListener = null;
  }
}
