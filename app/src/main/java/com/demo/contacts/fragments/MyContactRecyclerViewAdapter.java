package com.demo.contacts.fragments;

import java.util.List;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demo.contacts.R;
import com.demo.contacts.fragments.ContactsFragment.OnListFragmentInteractionListener;
import com.demo.contacts.models.Contact;


/**
 * {@link RecyclerView.Adapter} that can display a {@link Contact} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyContactRecyclerViewAdapter
    extends RecyclerView.Adapter<MyContactRecyclerViewAdapter.ViewHolder>
{

  private final List<Contact> mValues;

  private final OnListFragmentInteractionListener mListener;

  public MyContactRecyclerViewAdapter(List<Contact> items, OnListFragmentInteractionListener listener)
  {
    mValues = items;
    mListener = listener;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
  {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.fragment_contact, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position)
  {
    holder.mItem = mValues.get(position);
    holder.mNameView.setText(mValues.get(position).name.first);
    holder.mPhoneView.setText(mValues.get(position).phone);

    holder.mView.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        if (null != mListener)
        {
          // Notify the active callbacks interface (the activity, if the
          // fragment is attached to one) that an item has been selected.
          mListener.onListFragmentInteraction(holder.mItem);
        }
      }
    });
  }

  @Override
  public int getItemCount()
  {
    return mValues.size();
  }

  public class ViewHolder
      extends RecyclerView.ViewHolder
  {

    public final View mView;

    public final TextView mNameView;

    public final TextView mPhoneView;

    public Contact mItem;

    public ViewHolder(View view)
    {
      super(view);
      mView = view;
      mNameView = (TextView) view.findViewById(R.id.name);
      mPhoneView = (TextView) view.findViewById(R.id.phone);
    }

    @Override
    public String toString()
    {
      return super.toString() + " '" + mPhoneView.getText() + "'";
    }
  }
}
