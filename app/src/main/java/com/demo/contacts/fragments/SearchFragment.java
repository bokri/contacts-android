package com.demo.contacts.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.demo.contacts.R;
import com.demo.contacts.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment
    extends Fragment implements OnClickListener
{

  EditText nameEditText;
  Button searchButton;
  TextView result;

  public SearchFragment()
  {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState)
  {
    // Inflate the layout for this fragment
    View rootView = inflater.inflate(R.layout.fragment_search, container, false);

    nameEditText = (EditText) rootView.findViewById(R.id.editText);
    searchButton = (Button) rootView.findViewById(R.id.button);
    result = (TextView) rootView.findViewById(R.id.result);

    searchButton.setOnClickListener(this);

    return rootView;
  }

  @Override
  public void onClick(View view)
  {
    if(view == searchButton) {

      String nameText = nameEditText.getText().toString();
      if(Utils.contactExists(nameText))
        result.setText("Contact exists");
      else
        result.setText("No contact found");
    }
  }
}
