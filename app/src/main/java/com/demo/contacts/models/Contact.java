package com.demo.contacts.models;

import java.io.Serializable;

/**
 * @author Aymen Bokri
 * @since aymenbokri
 */

public class Contact
    implements Serializable
{
  public String gender;
  public Name name;
  public Location location;
  public String email;
  public String username;
  public String password;
  public String phone;
  public String cell;
  public String SSN;
  public Picture picture;

  public Contact(String gender, Name name, Location location, String email, String username, String password,
      String phone, String cell, String SSN, Picture picture)
  {
    this.gender = gender;
    this.name = name;
    this.location = location;
    this.email = email;
    this.username = username;
    this.password = password;
    this.phone = phone;
    this.cell = cell;
    this.SSN = SSN;
    this.picture = picture;
  }

  public Contact()
  {
    this.gender = "";
    this.name = new Name();
    this.location = new Location();
    this.email = "";
    this.username = "";
    this.password = "";
    this.phone = "";
    this.cell = "";
    this.SSN = "";
    this.picture = new Picture();
  }

  @Override
  public String toString()
  {
    return "Contact{" +
        "phone='" + phone + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }

    Contact contact = (Contact) o;

    return name.equals(contact.name);

  }

  @Override
  public int hashCode()
  {
    return name.hashCode();
  }
}
