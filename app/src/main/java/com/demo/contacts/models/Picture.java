package com.demo.contacts.models;

import java.io.Serializable;

/**
 * @author Aymen Bokri
 * @since aymenbokri
 */

public class Picture implements Serializable
{
  public String large;
  public String medium;
  public String thumbnail;

  public Picture(String large, String medium, String thumbnail)
  {
    this.large = large;
    this.medium = medium;
    this.thumbnail = thumbnail;
  }

  public Picture()
  {
    this.large = "";
    this.medium = "";
    this.thumbnail = "";
  }

  @Override
  public String toString()
  {
    return "Picture{" +
        "thumbnail='" + thumbnail + '\'' +
        '}';
  }
}
