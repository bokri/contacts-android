package com.demo.contacts.models;

import java.io.Serializable;

/**
 * @author Aymen Bokri
 * @since aymenbokri
 */

public class Location implements Serializable
{
  public String street;
  public String city;
  public String state;
  public String zip;


  public Location(String street, String city, String state, String zip)
  {
    this.street = street;
    this.city = city;
    this.state = state;
    this.zip = zip;
  }

  public Location()
  {
    this.street = "";
    this.city = "";
    this.state = "";
    this.zip = "";
  }

  @Override
  public String toString()
  {
    return "Location{" +
        "street='" + street + '\'' +
        '}';
  }
}
