package com.demo.contacts.models;

import java.io.Serializable;

/**
 * @author Aymen Bokri
 * @since 17.08.2017
 */

public class Name implements Serializable
{
  public String title;
  public String first;
  public String last;

  public Name(String title, String first, String last)
  {
    this.title = title;
    this.first = first;
    this.last = last;
  }

  public Name()
  {
    this.title = "";
    this.first = "";
    this.last = "";
  }

  @Override
  public String toString()
  {
    return "Name{" +
        "first='" + first + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }

    Name name = (Name) o;

    return first.equals(name.first);

  }

  @Override
  public int hashCode()
  {
    return first.hashCode();
  }
}
